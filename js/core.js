

idJornada = "";
// Initialize Firebase
const develop = {
  apiKey: "AIzaSyBEf9-h8CuW9cNjiJo8uLnMfbcBjgPxG9Y",
  authDomain: "developchamaclub.firebaseapp.com",
  databaseURL: "https://developchamaclub.firebaseio.com",
  projectId: "developchamaclub",
  storageBucket: "developchamaclub.appspot.com",
  messagingSenderId: "326557585019",
  appId: "1:326557585019:web:30b4aea596eb27fa74dcde"
};


const produccion = {
  apiKey: "AIzaSyC2dxeazyHNSTAuimIhFaVXt8AYZV3aub4",
  authDomain: "sorteoschama.firebaseapp.com",
  databaseURL: "https://sorteoschama.firebaseio.com",
  projectId: "sorteoschama",
  storageBucket: "sorteoschama.appspot.com",
  messagingSenderId: "214342016835",
  appId: "1:214342016835:web:68bede8357c4936f29d9f6"
};

firebase = firebase.initializeApp(produccion);
db = firebase.firestore();
idWinner = "";
idJornadaC = ""
idWinnerC = ""
db.collection("cuponeras").doc("osorno").onSnapshot(function (queryCup) {
  idJornada = queryCup.data().jornada
  idWinner = queryCup.data().id
})
db.collection("cuponeras").doc("coyhaique").onSnapshot(function (queryCup) {
  idJornadaC = queryCup.data().jornada
  idWinnerC = queryCup.data().id
})

local = "osorno"
listClientes = [];
listSucursales = [];
listCategorias = [];

function borrarTodosSorteos() {
  db.collection("sorteos").delete();
}



function restaurar(locals) {
  llaves = ""
  var r = confirm("Está seguro que desea restaurar" + locals + " ?");
  if (r == true) {
    var refLocal = db.collection("cuponeras").doc(locals);
    return refLocal.update({
      cuponera: "activa",
      pantalla: "listado"
    })
  }
}

function reiniciarGanadores(locals) {
  llaves = ""
  var r = confirm("Desea reiniciar los Ganadores  y la lista de los 5 clientes en sorteo de " + locals + " ?");
  if (r == true) {
    db.collection("clientes").where("sucursal", "==", locals).get().then((data) => {
      data.forEach((datos) => {
        llaves = datos.id;

        if (locals == datos.data().sucursal) {
          db.collection("clientes").doc(llaves).update({
            ganador: false
          })

        }

      })
      console.log("reiniciando los top 5")
      db.collection("clientesSorteando").get().then((data) => {
        data.forEach((datos) => {
         
          llaves = datos.data().id;
          llaves2 = datos.id;
          console.log(llaves2)
           
              db.collection("clientesSorteando").doc(llaves2).update({
                nombre: "",
                apellido: "",
                rut: "",
                hora: "",
                sucursal: "",
                categoria: "",
                id: "",
                estado: false,
                minuto: "",
                ganador: "",
                deshabilitado: "",
              }).then(()=>{
  
                if(llaves!=""){
                  console.log("llaves:"+llaves)
                  db.collection("clientes").doc(llaves).update({
                    estado:false,
                    hora:""
                  }).then(()=>{
                    console.log("ñieee")
                })
                }
              
            })
  
         
  
        })
  
      })
    })
  }
}

function iniciarCupones(cupp) {
db.collection("clientes").get().then((data) => {
      data.forEach((datos) => {
        llave = datos.id;
          if (datos.data().sucursal == cupp) {
              db.collection("clientes").doc(llave).update({
                  estado: false,
                  cupon: false,
                  monto: 0,
                  fecha: "",
                  hora: ""
              })
          }

      })
 console.log("cupones iniciados")
  })
}

function borrarSorteos (){
  db.collection("sorteos").get().then((data) => {
      data.forEach((datos) => {
          llave = datos.id;
          db.collection("sorteos").doc(llave).delete()
      })
      console.log("sorteos reiniciadoss")
  })
}
function iniciarJornada(locc){
  password = prompt("Para iniciar ingrese la pass");
  if (password == "8521") {
      db.collection("jornadaCuponeras").add({
          monto: 0
      }).then((dato) => {
         jornada = dato.id;
         cuponeraEdit = db.collection("cuponeras").doc(locc);
          return cuponeraEdit.update({
              cuponera: "iniciando",
              jornada: jornada,
              nombre: "",
              fecha: obtenerFecha(),
              rut: "",
              id: "",
              estado: true
          }).then(() => {
              db.collection("jornadaCuponeras").add({
                  monto: 0
              }).then((dato) => {
                  jornada = dato.id;
                  console.log("borrandoSorteos")
                  iniciarCupones("osorno");
                  borrarSorteos()
              })
          })
      })
  } else {

  }

}


function iniciarLocal(locall) {
  var r = confirm("Desea Iniciar la cuponera de :" + locall + " ?");
  if (r == true) {

    db.collection("jornadaCuponeras").add({
      monto: 0
    }).then((dato) => {


      cuponeraEdit = db.collection("cuponeras").doc(locall);
      cuponeraEdit.update({
        estado: true,
        cuponera: "activa",
        jornada: dato.id,
        nombre: "",
        rut: "",
        id: ""
      }).then(() => {
        db.collection("sorteos").get().then((data) => {
          data.forEach((datos) => {
            llavec = datos.id;
            db.collection("sorteos").doc(llavec).delete()
          })
          alert("Local Aperturado");
          iniciarCupones(locall);
          dibujarTablaLocales();
        })
      })
    })

  }
}
function cerrarLocal(locall) {
  var r = confirm("Desea cerrar la cuponera de :" + locall + " ?");
  if (r == true) {
    cuponeraEdit = db.collection("cuponeras").doc(locall);
    cuponeraEdit.update({
      cuponera: "inactiva",
      jornada: '',
      pantalla: 'listado',
      nombre: "",
      rut: "",
      id: "",
      estado: false
    }).then(() => {
      alert("Local Cerrado!");
      dibujarTablaLocales()
    })
  }




}


function resetTopCinco(locals) {
  llaves = ""
  var r = confirm("Desea reiniciar los clientes que entran al sorteo de " + locals + " ?");
  if (r == true) {
    db.collection("clientesSorteando").get().then((data) => {
      data.forEach((datos) => {
       
        llaves = datos.data().id;
        llaves2 = datos.id;
        console.log(llaves2)
         
            db.collection("clientesSorteando").doc(llaves2).update({
              nombre: "",
              apellido: "",
              rut: "",
              hora: "",
              sucursal: "",
              categoria: "",
              id: "",
              estado: false,
              minuto: "",
              ganador: "",
              deshabilitado: "",
            }).then(()=>{

              if(llaves!=""){
                console.log("llaves:"+llaves)
                db.collection("clientes").doc(llaves).update({
                  estado:false,
                  hora:""
                }).then(()=>{
                  console.log("ñieee")
              })
              }
            
          })

       

      })

    })
  }
}

function reiniciarClientes(locals) {
  llaves = ""
  var r = confirm("Desea reiniciar los clientes de " + locals + " ?");
  if (r == true) {
    db.collection("clientes").where("sucursal", "==", locals).get().then((data) => {
      data.forEach((datos) => {
        llaves = datos.id;

        if (locals == datos.data().sucursal) {
          db.collection("clientes").doc(llaves).update({
            estado:false,
            cupon:false,
            hora:""
          })

        }

      })

    })
  }
}

function abrirModalSorteos(nombre, rut, id, sucursal) {
  $('#cuerpoTablaModal').html('');

  cargarTablaModal(nombre, rut, id, sucursal)
  $('#localModal').html(sucursal)
}

function cargarTablaModal(nombre, rut, id, sucursal) {
  db.collection("sorteos").orderBy("hora", "desc").get()
    .then(function (queryCup) {
      contenidoCoyhaique = "";
      queryCup.forEach(function (docCup) {
        if (docCup.data().tipo == "minutoFeliz") {

        } else {
          if (docCup.data().local == sucursal) {
            clase = ''
            hora = '';
            hora = docCup.data().hora;
            if (docCup.data().estado == true) {
              status = ""
              if (docCup.data().nombre != "") {
                clase = 'bg-success'
                status = "color:white;"
              }

              trof = `<i class="fas fa-trophy" style="font-size:130%;" onclick="selectCli('` + nombre + `','` + rut + `','` + id + `','` + sucursal + `','` + docCup.id + `')"></i> `
            } else {
              clase = ''
              trof = ` `
              status = "text-decoration: line-through;"
            }

            ganadorC = ""

            contenidoCoyhaique += `
        <tr class="`+ clase + `" style="` + status + `">
        <td > `+ docCup.data().hora + `<p> ` + docCup.data().premio + `</p></td>
        <td > `+ docCup.data().nombre + `</td>
        <td> <button class="btn btn-danger" onclick="eliminarSorteo('`+ docCup.id + `')">X</button>
            `+ trof + ` </td>
        </tr>`

          }
        }


      });
      $('#cuerpoTablaModal').html(contenidoCoyhaique);
      $('#modalSorteos').modal('show');

    });

}

function selectCli(nombre, rut, idCliente, sucursal, idSorteo) {
  console.log(rut+"rut seleect")
  $('#modalSorteos').modal('hide')
  db.collection("sorteos").doc(idSorteo).update({
    modalidadSorteo: 'manual',
    ganadorCliente: nombre,
    id: idCliente,
    nombre: nombre,
    rut: rut.trim(),
    rutGanador: rut.trim()
  }).then(function () {
    console.log("Sorteo actualizado");
  })
}